package com.adrianokerber.marvelheroes.model;

import io.realm.RealmObject;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class ComicORM extends RealmObject {

    private String resourceURI;
    private String name;

    public String getResourceURI() {
        return resourceURI;
    }

    public void setResourceURI(String resourceURI) {
        this.resourceURI = resourceURI;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
