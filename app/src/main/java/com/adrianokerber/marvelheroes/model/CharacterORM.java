package com.adrianokerber.marvelheroes.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class CharacterORM extends RealmObject {

    public static String ID = "id";

    @PrimaryKey
    private int id;
    private String name;
    private String description;
    private ImageORM thumbnail;
    private RealmList<ComicORM> comics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImageORM getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(ImageORM thumbnail) {
        this.thumbnail = thumbnail;
    }

    public RealmList<ComicORM> getComics() {
        return comics;
    }

    public void setComics(RealmList<ComicORM> comics) {
        this.comics = comics;
    }
}
