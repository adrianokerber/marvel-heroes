package com.adrianokerber.marvelheroes.model;

import io.realm.RealmObject;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class ImageORM extends RealmObject {

    private String path;
    private String extension;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
