package com.adrianokerber.marvelheroes.presenter.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.adrianokerber.marvelheroes.R;
import com.adrianokerber.marvelheroes.model.CharacterORM;
import com.adrianokerber.marvelheroes.presenter.activity.DetailActivity;
import com.adrianokerber.marvelheroes.presenter.api.ImageDownloader;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class HeroAdapter extends RecyclerView.Adapter<HeroAdapter.ViewHolder> {

    private RealmResults<CharacterORM> characterList;

    // To update the list data
    private Realm realm;
    private RealmChangeListener<RealmResults<CharacterORM>> realmListener;

    // Holder that matches the model with the view
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.content);
        }
    }

    public HeroAdapter(RealmResults<CharacterORM> queryResults) {
        realm = Realm.getDefaultInstance();

        characterList = queryResults;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        realmListener = listener -> this.notifyDataSetChanged();
        characterList.addChangeListener(realmListener);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        // Stop watching list updates on Realm
        if(realm != null) {
            characterList.removeChangeListener(realmListener);
            realm.close();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View couponView = inflater.inflate(R.layout.adapter_hero, parent, false);

        // Return a new holder instance
        return new ViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View characterView = holder.view;
        CharacterORM hero = characterList.get(position);
        final Context context = characterView.getContext();

        if (hero == null) {
            return;
        }

        // Set title
        TextView nameTextView = characterView.findViewById(R.id.name);
        nameTextView.setText(hero.getName());
        // Set image
        ImageView imageView = characterView.findViewById(R.id.image);
        String imageUrl = hero.getThumbnail().getPath()
                + "/"
                + "portrait_small" // File size
                + "." + hero.getThumbnail().getExtension();
        ImageDownloader.load(imageUrl, imageView);

        // Set click behaviour
        characterView.setClickable(true);
        characterView.setFocusable(true);
        characterView.setOnClickListener(view -> {
            // Open details screen
            Intent intent = new Intent(context, DetailActivity.class);
            intent.putExtra(DetailActivity.ID, hero.getId());
            context.startActivity(intent);
        });
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return characterList.size();
    }
}
