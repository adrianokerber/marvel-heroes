package com.adrianokerber.marvelheroes.presenter.api;

import android.content.Context;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class ImageDownloader {
    private static Picasso picasso; // The image downloader

    /**
     * Initializer for class
     */
    public static void init(Context context) {
        if (picasso == null) {
            picasso = new Picasso.Builder(context)
                    .build();
        }
    }

    /**
     * Request image download to ImageView
     * @param imageUrl
     * @param target
     */
    public static void load(String imageUrl, ImageView target) {
        picasso.load(imageUrl)
                .into(target);
    }
}
