package com.adrianokerber.marvelheroes.presenter.activity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.adrianokerber.marvelheroes.R;
import com.adrianokerber.marvelheroes.model.CharacterORM;
import com.adrianokerber.marvelheroes.model.ComicORM;
import com.adrianokerber.marvelheroes.model.ImageORM;
import com.adrianokerber.marvelheroes.presenter.adapter.HeroAdapter;
import com.arnaudpiroelle.marvel.api.MarvelApi;
import com.arnaudpiroelle.marvel.api.objects.Character;
import com.arnaudpiroelle.marvel.api.objects.ComicSummary;
import com.arnaudpiroelle.marvel.api.objects.ref.DataWrapper;
import com.arnaudpiroelle.marvel.api.services.async.CharactersAsyncService;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();
    private CharactersAsyncService charactersService;
    private SwipeRefreshLayout reload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        charactersService = MarvelApi.getService(CharactersAsyncService.class);

        updateData();

        HeroAdapter adapter;
        try (Realm realm = Realm.getDefaultInstance()) {
            adapter = new HeroAdapter(realm
                    .where(CharacterORM.class)
                    .findAll());
        }
        // Get the recycler view
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        // Attach the adapter to the RecyclerView to populate items
        recyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Refresh logic for data from API
        reload = findViewById(R.id.reloadSwipeRefresh);
        reload.setOnRefreshListener(() -> {
            reload.setRefreshing(true);
            updateData();
        });
    }

    private void updateData() {
        try {
            charactersService.listCharacter(new Callback<DataWrapper<Character>>() {

                /**
                 * Successful HTTP response.
                 *
                 * @param characterDataWrapper
                 * @param response
                 */
                @Override
                public void success(DataWrapper<Character> characterDataWrapper, Response response) {
                    try (Realm realm = Realm.getDefaultInstance()) {
                        realm.executeTransaction(realm1 -> {
                            // Clear old data
                            realm1.deleteAll();
                            // Load new data
                            for (Character character : characterDataWrapper.getData().getResults()) {
                                // Character
                                CharacterORM characterORM = realm1.where(CharacterORM.class).
                                        equalTo(CharacterORM.ID, character.getId()).findFirst();
                                if (characterORM == null) {
                                    characterORM = realm1.createObject(CharacterORM.class, character.getId());
                                }
                                characterORM.setName(character.getName());
                                characterORM.setDescription(character.getDescription());
                                // Thumbnail
                                ImageORM thumbnailORM = realm1.createObject(ImageORM.class);
                                thumbnailORM.setPath(character.getThumbnail().getPath());
                                thumbnailORM.setExtension(character.getThumbnail().getExtension());
                                characterORM.setThumbnail(thumbnailORM);
                                // Comics
                                RealmList<ComicORM> comics = new RealmList<>();
                                for (ComicSummary comicSummary : character.getComics().getItems()) {
                                    ComicORM comicORM = realm1.createObject(ComicORM.class);
                                    comicORM.setName(comicSummary.getName());
                                    comicORM.setResourceURI(comicSummary.getResourceURI());
                                    comics.add(comicORM);
                                }
                                characterORM.setComics(comics);
                            }
                        });

                        // Stop refreshing
                        reload.setRefreshing(false);
                    }
                }

                /**
                 * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
                 * exception.
                 *
                 * @param error
                 */
                @Override
                public void failure(RetrofitError error) {
                    Log.e(TAG, error.toString());

                    // Stop refreshing
                    reload.setRefreshing(false);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, e.getLocalizedMessage());

            // Stop refreshing
            reload.setRefreshing(false);
        }
    }
}
