package com.adrianokerber.marvelheroes.presenter.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.adrianokerber.marvelheroes.R;
import com.adrianokerber.marvelheroes.model.ComicORM;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class ComicAdapter extends RecyclerView.Adapter<ComicAdapter.ViewHolder> {

    private RealmResults<ComicORM> comicList;

    // To update the list data
    private Realm realm;
    private RealmChangeListener<RealmResults<ComicORM>> realmListener;

    // Holder that matches the model with the view
    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;

        public ViewHolder(View itemView) {
            super(itemView);

            view = itemView.findViewById(R.id.content);
        }
    }

    public ComicAdapter(RealmResults<ComicORM> queryResults) {
        realm = Realm.getDefaultInstance();

        comicList = queryResults;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);

        realmListener = listener -> this.notifyDataSetChanged();
        comicList.addChangeListener(realmListener);
    }

    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);

        // Stop watching list updates on Realm
        if(realm != null) {
            comicList.removeChangeListener(realmListener);
            realm.close();
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        // Inflate the custom layout
        View couponView = inflater.inflate(R.layout.adapter_comic, parent, false);

        // Return a new holder instance
        return new ViewHolder(couponView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        View comicView = holder.view;
        ComicORM comic = comicList.get(position);

        if (comic == null) {
            return;
        }

        // Set title
        TextView textView = comicView.findViewById(R.id.title);
        textView.setText(comic.getName());
    }

    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return comicList.size();
    }
}
