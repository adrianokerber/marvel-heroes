package com.adrianokerber.marvelheroes.presenter.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.adrianokerber.marvelheroes.R;
import com.adrianokerber.marvelheroes.model.CharacterORM;
import com.adrianokerber.marvelheroes.presenter.adapter.ComicAdapter;
import com.adrianokerber.marvelheroes.presenter.api.ImageDownloader;

import io.realm.Realm;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class DetailActivity extends AppCompatActivity {

    public static final String ID = "id";

    private Realm realm;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail);

        realm = Realm.getDefaultInstance();

        int id = getIntent().getIntExtra(ID, -1);
        CharacterORM characterORM = realm.where(CharacterORM.class)
                .equalTo(CharacterORM.ID, id)
                .findFirst();

        // Name
        TextView name = findViewById(R.id.name);
        name.setText(characterORM.getName());
        // Thumbnail
        ImageView thumbnail = findViewById(R.id.thumbnail);
        String imageUrl = characterORM.getThumbnail().getPath()
                + "/"
                + "portrait_fantastic" // File size
                + "." + characterORM.getThumbnail().getExtension();
        ImageDownloader.load(imageUrl, thumbnail);
        // Description
        TextView description = findViewById(R.id.description);
        description.setText(characterORM.getDescription());

        // Load comics list
        ComicAdapter adapter = new ComicAdapter(characterORM
                .getComics()
                .where()
                .findAll());
        // Get the recycler view
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        // Attach the adapter to the RecyclerView to populate items
        recyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (realm != null) {
            realm.close();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
