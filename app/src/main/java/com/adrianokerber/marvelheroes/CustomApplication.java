package com.adrianokerber.marvelheroes;

import android.support.multidex.MultiDexApplication;

import com.adrianokerber.marvelheroes.presenter.api.ImageDownloader;
import com.arnaudpiroelle.marvel.api.MarvelApi;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Adriano Kerber on 8/25/18.
 */
public class CustomApplication extends MultiDexApplication {
    @Override
    public void onCreate() {
        super.onCreate();

        // Initialize RealmDB
        Realm.init(this);
        Realm.setDefaultConfiguration(
                new RealmConfiguration
                        .Builder()
                        .deleteRealmIfMigrationNeeded()
                        .build()
        );

        // Initialize third party Marvel API client
        MarvelApi.configure()
                .withApiKeys("ad6d386e4e723b9779cebd7d89f92a59",
                        "920a7b5d39e231c4a3ee047e8f75efca794634fa")
                .init();

        // Initialize image downloader
        ImageDownloader.init(this);
    }
}
