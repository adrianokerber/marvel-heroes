# Marvel Heroes App
Developed by [Adriano Kerber](mailto:kerberpro@gmail.com)

This app was developed to display Marvel's heroes and their profiles.

## Requirements

* Android Studio (v3.0.0 or above)

## Setup

Just clone this repository and open the project folder with the Android Studio IDE.

### Warn

Please disable instant run if using debug mode, because it's been issued that can cause a crash.
[Full issue description here](https://issuetracker.google.com/issues/37126388)

## Third Party libraries

* [Marvel's API client](https://github.com/ArnaudPiroelle/marvel-api)
* [Picasso](http://square.github.io/picasso/)
* [Realm DB](https://realm.io/products/realm-database/)