# Change log

### Next

* Add unit test

### 2018.08.1

Created project and addressed main functionalities

* Added characters list
* Added Marvels' API
* Added details screen
* Added Realm DB
* Added refresh on main screen